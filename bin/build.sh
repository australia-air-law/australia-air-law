#!/bin/sh

set -e

if ! type wget >/dev/null ; then
  >&2 echo "Missing: wget" >&2
  exit 1
fi

if ! type pdftk >/dev/null ; then
  >&2 echo "Missing: pdftk" >&2
  exit 1
fi

if ! type qpdf >/dev/null ; then
  >&2 echo "Missing: qpdf" >&2
  exit 1
fi

script_dir=$(dirname "$0")
share_dir=${script_dir}/../share

if [ -z "${1}" ]
then
  hostLink="N/A"
else
  hostLink=${1}
fi

if [ -z "${2}" ]
then
  sourceHost="N/A"
else
  sourceHost=${2}
fi

if [ -z "${3}" ]
then
  lastTime="N/A"
else
  lastTime=${3}
fi

if [ -z "${4}" ]
then
  lastUser="N/A"
else
  lastUser=${4}
fi

if [ -z "${5}" ]
then
  lastEmail="N/A"
else
  lastEmail=${5}
fi

if [ -z "${6}" ]
then
  revision="N/A"
else
  revision=${6}
fi

if [ -z "${7}" ]
then
  access="N/A"
else
  access=${7}
fi

if [ -z "${8}" ]
then
  outfile=${script_dir}/../public/index.html
else
  outfile=${8}
fi

if [ -z "${9}" ]
then
  download_dir=${script_dir}/../public
else
  download_dir=${9}
fi

if [ -z "${10}" ]
then
  index_file=${download_dir}/index.txt
else
  index_file=${10}
fi

if [ -z "${11}" ]
then
  extract_dir=${download_dir}/extract
else
  extract_dir=${11}
fi

if [ -z "${12}" ]
then
  aip_ersa="08SEP2022"
else
  aip_ersa=${12}
fi

script_dir=$(dirname "$0")
${script_dir}/download-law.sh ${download_dir} ${index_file} ${aip_ersa}
${script_dir}/extract-law.sh ${download_dir} ${index_file} ${extract_dir}
${script_dir}/index.hs "${script_dir}/../public/" "${index_file}" "${outfile}" "${hostLink}" "${sourceHost}" "${lastTime}" "${lastUser}" "${lastEmail}" "${revision}" "${access}"
rsync -aH "${share_dir}/" "${download_dir}"
