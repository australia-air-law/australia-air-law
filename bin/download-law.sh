#!/bin/sh

set -e

if ! type wget >/dev/null ; then
  >&2 echo "Missing: wget" >&2
  exit 1
fi

if ! type qpdf >/dev/null ; then
  >&2 echo "Missing: qpdf" >&2
  exit 1
fi

if [ -z "$3" ]
then
  >&2 echo "arguments: <download-dir> <index-file> <aip-date>"
  exit 33
else
  download_dir=${1}
  index_file=${2}
  aip_ersa=${3}
fi

isInForce() {
  if [ $# -lt 1 ]; then
    echo "arguments(1) <legislation-dir>"
    exit 9
  else
    wget --ciphers DEFAULT@SECLEVEL=1 -e robots=off https://www.legislation.gov.au/Details/${1} -qO - | grep -q "<span\s*.*class=\"GreenText\">In force</span>" && true
    return $?
  fi
}

downloadLegislation() {
  if [ $# -lt 6 ]; then
    echo "arguments(6) <legislation-dir> <legislation-hash> <out-dir> <out-file> <header> <name>"
    exit 9
  else
    isInForce ${1} && true
    if [ "$?" -eq 1 ]; then
      >&2 echo "Legislation not in force ${1} (${5} ${6})"
      exit 12
    else
      out_dir=${download_dir}/${3}
      out_file=${out_dir}/${4}
      mkdir -p ${out_dir}
      wget --ciphers DEFAULT@SECLEVEL=1 -e robots=off https://www.legislation.gov.au/Details/${1}/${2} -O ${out_file}
      qpdf --check ${out_file}
      if [ "$?" != 0 ]; then
        >&2 echo "PDF file failed check ${out_file}"
        exit 13
      fi

      echo "${5},${6},${out_file}" >> ${index_file}
    fi
  fi
}

downloadAllLegislation() {
  # CAR 1988
  downloadLegislation "F2021C01179" "eb679a1d-c6d2-42da-981b-fbdc40a62fc9" car1988 car1988-volume1.pdf "Civil Aviation Regulations 1988" "Volume 1"
  downloadLegislation "F2021C01179" "9a983cd5-e489-4254-8135-152ab6d407ab" car1988 car1988-volume2.pdf "Civil Aviation Regulations 1988" "Volume 2"

  # CASR 1998
  downloadLegislation "F2022C00697" "f5b346ed-0256-4782-b275-d523b1fd6bf5" casr1998 casr1998-volume1.pdf "Civil Aviation Safety Regulations 1998" "Volume 1"
  downloadLegislation "F2022C00697" "84a88238-1525-4a82-9855-802eaf226228" casr1998 casr1998-volume2.pdf "Civil Aviation Safety Regulations 1998" "Volume 2"
  downloadLegislation "F2022C00697" "1c434490-cf8c-48ec-b85e-169aec2164d6" casr1998 casr1998-volume3.pdf "Civil Aviation Safety Regulations 1998" "Volume 3"
  downloadLegislation "F2022C00697" "5783326d-9e1d-4788-821f-856248dae05f" casr1998 casr1998-volume4.pdf "Civil Aviation Safety Regulations 1998" "Volume 4"
  downloadLegislation "F2022C00697" "68730cd0-823c-4275-9a97-91316af5a46a" casr1998 casr1998-volume5.pdf "Civil Aviation Safety Regulations 1998" "Volume 5"

  # Part 61 Manual of Standards
  downloadLegislation "F2021C00449" "bdc418cc-7705-480e-84c3-4b719381d4d9" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume1.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 1"
  downloadLegislation "F2021C00449" "0865d40b-7b7f-4e5f-ba60-a69bd1c09e09" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume2.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 2"
  downloadLegislation "F2021C00449" "273699d4-594d-4cc7-8a9f-ee2267de2bc6" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume3.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 3"
  downloadLegislation "F2021C00449" "72e6c9d2-da35-436f-9e6b-e821b08700b5" casr1998/part61-manual-of-standards casr1998-part61-manual-of-standards-volume4.pdf "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards" "Volume 4"

  # Part 101 Manual of Standards
  downloadLegislation "F2022C00594" "55344fa7-28c1-407f-8d14-6f6b0fa98015" casr1998/part101-manual-of-standards casr1998-part101-manual-of-standards.pdf "Civil Aviation Safety Regulations 1998 Part 101 Manual of Standards" "Unmanned Aircraft and Rockets Manual of Standards 2019 (as amended)"

  # Part 91 Manual of Standards
  downloadLegislation "F2022C00789" "bbea5568-cca2-4397-b24b-a2894442a47e" casr1998/part91-manual-of-standards casr1998-part91-manual-of-standards.pdf "Civil Aviation Safety Regulations 1998 Part 91 Manual of Standards" "Complete"

  # CAO
  downloadLegislation "F2006C00266" "a4b0688f-a76f-4acf-85c8-361ccec50dc9" cao cao20.2.pdf "Civil Aviation Order" "20.2 Air service operations - safety precautions before flight"
  downloadLegislation "F2005B00776" "25fc789b-0838-44b2-badc-ad5710b9b8dd" cao cao20.3.pdf "Civil Aviation Order" "20.3 Air service operations - marshalling and parking of aircraft"
  downloadLegislation "F2005B00777" "5ac582a3-fde3-4cbd-b97f-e26173d18dec" cao cao20.4.pdf "Civil Aviation Order" "20.4 Provision and use of oxygen and protective breathing equipment"
  downloadLegislation "F2010C00633" "643aa2b5-b76b-460f-89a8-6d2f280fd158" cao cao20.6.pdf "Civil Aviation Order" "20.6 Continuation of flight with 1 or more engines inoperative"
  downloadLegislation "F2005B00781" "fa078501-66e6-49b1-9762-987ce34985e0" cao cao20.7.1.pdf "Civil Aviation Order" "20.7.1 Aeroplane weight limitations - Aeroplanes above 5 700 kg - all operations (piston-engined)"
  downloadLegislation "F2014C01352" "09aa949a-8052-4b24-be66-16d49e066c0c" cao cao20.7.1B.pdf "Civil Aviation Order" "20.7.1B Aeroplane weight and performance limitations - specified aeroplanes above 5 700 kg or 2 722 kg if driven by 2 or more jet engines - all operations as amended"
  downloadLegislation "F2005B00785" "3f6ae5d7-9b53-4099-9b8b-962d639d5a79" cao cao20.7.2.pdf "Civil Aviation Order" "20.7.2 Aeroplane weight and performance limitations - aeroplanes not above 5 700 kg - regular public transport operations"
  downloadLegislation "F2011C00881" "714cfb88-8fc7-488a-a4eb-76f9da942779" cao cao20.9.pdf "Civil Aviation Order" "20.9 Air service operations - precautions in refuelling engine and ground radar operations"
  downloadLegislation "F2005B00788" "eac644fa-e95e-4740-9135-dad16bec3551" cao cao20.10.pdf "Civil Aviation Order" "20.10 Hot Refuelling - helicopters"
  downloadLegislation "F2005B00790" "a3b4a800-da95-46ea-aa1e-7fa8e0b0e325" cao cao20.10.1.pdf "Civil Aviation Order" "20.10.1 Hot Refuelling - turbine engine aeroplane engaged in aerial work or private operations"
  downloadLegislation "F2009C00093" "6aee956d-7d73-42ea-9353-2bbd336ebad8" cao cao20.11.pdf "Civil Aviation Order" "20.11 Emergency and life saving equipment and passenger control in emergencies"
  downloadLegislation "F2005B00792" "db8702aa-e05a-4033-a1c0-09579bd2f39a" cao cao20.13.pdf "Civil Aviation Order" "20.13 Air service operations - cockpit check systems"
  downloadLegislation "F2018C00462" "652f6e88-8cc9-43f8-b303-a37de880ee04" cao cao20.16.1.pdf "Civil Aviation Order" "20.16.1 Air service operations - loading - general as amended"
  downloadLegislation "F2005B00989" "fc2d157a-d6ab-46a3-974d-9fe0fda4d38e" cao cao20.16.2.pdf "Civil Aviation Order" "20.16.2 Air service operations - loading - general"
  downloadLegislation "F2021C00800" "444b5005-df29-4578-b6ae-cd80574a5d64" cao cao20.16.3.pdf "Civil Aviation Order" "20.16.3 Air service operations - carriage of persons"
  downloadLegislation "F2005B00799" "ba606949-8f4e-4df4-8333-158529b0bea1" cao cao20.17.pdf "Civil Aviation Order" "20.17 Air service Operations - use of military aerodromes by civil aircraft"
  downloadLegislation "F2020C00611" "71d04901-3f45-4778-9ffc-8d2e54d01381" cao cao20.18.pdf "Civil Aviation Order" "20.18 Aircraft equipment - basic operational requirements"
  downloadLegislation "F2015L00662" "ede488d3-db94-488f-af37-d4a860131b7e" cao cao20.21.pdf "Civil Aviation Order" "20.21"
  downloadLegislation "F2005B00807" "44b3307a-3c5c-457e-bfb0-414b89c7bbd4" cao cao20.22.pdf "Civil Aviation Order" "20.22 Taxiing of aircraft by persons other than licensed pilots"
  downloadLegislation "F2018C00889" "54cd4368-9e5e-4642-a428-f3d77c7a1604" cao cao20.91.pdf "Civil Aviation Order" "20.91 Instructions and directions for performance-based navigation"
  downloadLegislation "F2015C00073" "e37f5c49-e1b6-47b8-a336-3bcd848103a9" cao cao29.1.pdf "Civil Aviation Order" "29.1 Air service operations - aircraft engaged in aerial stock mustering operations - low flying permission"
  downloadLegislation "F2015C00116" "267b4603-9652-4908-80e5-1e50b4ba06a9" cao cao29.2.pdf "Civil Aviation Order" "29.2 Air service operations - night flying training"
  downloadLegislation "F2005B00825" "477bc394-0e4a-446c-b89a-484b4f307fa4" cao cao29.3.pdf "Civil Aviation Order" "29.3 Air service operations - aeroplanes engaged in agricultural operation - night aerial spraying"
  downloadLegislation "F2015C00051" "c5add01b-f8e6-479c-b661-1a704f35cf61" cao cao29.4.pdf "Civil Aviation Order" "29.4 Air Displays"
  downloadLegislation "F2015C00054" "719e66d7-1467-468c-a0aa-258a374650b3" cao cao29.5.pdf "Civil Aviation Order" "29.5 Air service operations - miscellaneous dropping of articles from aircraft in flight"
  downloadLegislation "F2014C01385" "d3ab4461-441f-4896-8759-7c092c9fdbd3" cao cao29.6.pdf "Civil Aviation Order" "29.6 Air service operations - helicopter external sling load operations"
  downloadLegislation "F2005B00838" "98faad93-75ef-4782-8847-26f1a4c2c2fb" cao cao29.8.pdf "Civil Aviation Order" "29.8 Ferry flight of aeroplanes with 1 engine inoperative"
  downloadLegislation "F2015C00073" "e37f5c49-e1b6-47b8-a336-3bcd848103a9" cao cao29.10.pdf "Civil Aviation Order" "29.10 Air service operations - aircraft engaged in aerial stock mustering operations - low flying permission"
  downloadLegislation "F2015C00074" "2c0ecab8-e079-4af8-91e5-380b63bc72dd" cao cao29.11.pdf "Civil Aviation Order" "29.11 Air service operations - helicopter winching and rappelling operations"
  downloadLegislation "F2015C00077" "e16dd439-0969-4d93-80e8-b2bfca2ba3f2" cao cao40.2.2.pdf "Civil Aviation Order" "40.2.2 Balloon grade of night V.F.R. rating"
  downloadLegislation "F2019C00673" "fcdf9d9c-ef39-4ea9-a5db-276666aaa802" cao cao40.7.pdf "Civil Aviation Order" "40.7 Aircraft endorsements (balloons) and flight instructor (balloons) ratings as amended"
  downloadLegislation "F2005B00648" "b7e04f41-ce37-42c7-b7d7-ae37a593121a" cao cao45.0.pdf "Civil Aviation Order" "45.0 Flight crew standards - synthetic trainers - general"
  downloadLegislation "F2021C01239" "96cb8b56-6535-4962-a65b-dde12c2d30a3" cao cao48.1.pdf "Civil Aviation Order" "48.1"
  downloadLegislation "F2014L01763" "4df2948e-1896-471a-ba6f-c68c00cc14fb" cao cao82.0.pdf "Civil Aviation Order" "82.0"
  downloadLegislation "F2021C01241" "54be3d26-8aec-49bf-808e-75d5c6e92939" cao cao82.1.pdf "Civil Aviation Order" "82.1 Conditions on Air Operators' Certificates authorising charter operations and aerial work operations"
  downloadLegislation "F2021C01277" "cf14e9f8-3e94-4589-b185-90b1e322b43f" cao cao82.7.pdf "Civil Aviation Order" "82.7 Air Operators' Certificates authorising aerial work operations and charter operations in balloons"
  downloadLegislation "F2013C00286" "2556e3b7-a03a-460c-bffe-03afb1aa0431" cao cao95.4.1.pdf "Civil Aviation Order" "95.4.1 Exemption from provisions of the Civil Aviation Regulations 1988 - gliders engaged in charter operations"
  downloadLegislation "F2010C00712" "11c3bb73-7873-4bdc-bf8e-ae1c2aa2e5c6" cao cao95.7.pdf "Civil Aviation Order" "95.7 Exemption from provisions of the Civil Aviation Regulations 1988 - helicopters"
  downloadLegislation "F2005B00890" "53325a5f-dd62-4268-97a4-6c9248ddc9c2" cao cao95.7.2.pdf "Civil Aviation Order" "95.7.2 Exemption of certain helicopters engaged in rappelling sling load or winching operations from compliance with certain flight manual limitations"
  downloadLegislation "F2005B00892" "53ae5721-60a2-4cfc-8d04-90f4d8647bb0" cao cao95.7.3.pdf "Civil Aviation Order" "95.7.3 Exemption of certain helicopters engaged in transfer-ring marine pilots from compliance with subregulation 174B (2) of the Civil Aviation Regulations 1988"
  downloadLegislation "F2005B00894" "645872f1-4830-477e-a4a0-29631fd24de2" cao cao95.9.pdf "Civil Aviation Order" "95.9 Exemption of Australian aeroplanes from compliance with certain provisions of the Civil Aviation Regulations 1988 - demonstration flights outside Australia"
  downloadLegislation "F2015C00133" "772be197-ab76-45b4-b8bc-9fc819932c38" cao cao95.14.pdf "Civil Aviation Order" "95.14 Exemption from provisions of the regulations under the Civil Aviation Act 1988 - parasails and gyrogliders"
  downloadLegislation "F2005B00902" "384dd50b-5ff2-41e2-be66-c07a3c169219" cao cao95.19.pdf "Civil Aviation Order" "95.19 Exemption from provisions of the Civil Aviation Regulations 1988 - F/A 18 aircraft"
  downloadLegislation "F2005B00903" "3f652fe9-7dfc-47cc-8920-71f9b799cdb5" cao cao95.20.pdf "Civil Aviation Order" "95.20 Exemption from provisions of the Civil Aviation Regulations 1988 - operation of military (state) aircraft by civilian flight crew"
  downloadLegislation "F2005B00905" "c57865e1-ce98-4e2c-8a12-1b9429b47d1a" cao cao95.22.pdf "Civil Aviation Order" "95.22 Exemption from provisions of the Civil Aviation Regulations 1988 - float planes operating in prescribed access lanes"
  downloadLegislation "F2005B00906" "21e67191-91f7-459c-8155-84f3620f6630" cao cao95.23.pdf "Civil Aviation Order" "95.23 Exemption from subregulations 178(1) and (2) of the Civil Aviation Regulations 1988 - for offshore and coastal surveillance and search and rescue"
  downloadLegislation "F2005B00908" "1fc408f3-f4a6-4855-a849-eba1897a3d74" cao cao95.26.pdf "Civil Aviation Order" "95.26 Exemption from subregulations 178(1) and (2) of the Civil Aviation Regulations 1988 - for trial operations of supply dropping of search and rescue (SAR) stores at night"
  downloadLegislation "F2005B00910" "e35dfeda-a09b-4229-bda9-976db87e3dd7" cao cao95.27.pdf "Civil Aviation Order" "95.27 Exemption from provisions of the Civil Aviation Regulations 1988 - S-70A-9 helicopter"
  downloadLegislation "F2005B00911" "d432ec45-943b-4058-aefd-848aaefb1cf5" cao cao95.28.pdf "Civil Aviation Order" "95.28 Exemption from provisions of the Civil Aviation Regulations 1988 - S-70B-2 helicopter"
  downloadLegislation "F2005B00913" "0d78aa6e-08f7-464f-8b53-de39fa5b7ac8" cao cao95.29.pdf "Civil Aviation Order" "95.29 Exemption from provisions of the Civil Aviation Regulations 1988 - Pilatus PC9 aircraft"
  downloadLegislation "F2005B00917" "4fc4adb9-3e8a-454b-8f31-a2a94ecb7f4d" cao cao95.30.pdf "Civil Aviation Order" "95.30 Exemption from provisions of the Civil Aviation Regulations 1988 and the Civil Aviation Safety Regulations 1998 - British Aerospace MK 127 aircraft"
  downloadLegislation "F2005B00918" "90563cec-edb5-4d8f-baf6-19977e3e50d0" cao cao95.31.pdf "Civil Aviation Order" "95.31 Exemption from provisions of the Civil Aviation Regulations 1988 and the Civil Aviation Safety Regulations 1998 - Kaman Super Seasprite SH-2G(A) aircraft"
  downloadLegislation "F2007L04300" "1b92fe6a-e4a8-44ff-8d08-b32a22e51409" cao cao95.34.pdf "Civil Aviation Order" "95.34 NHIndustries NH90 Tactical Transport Helicopter"
  downloadLegislation "F2022C00648" "245fca64-a662-451b-8f5d-3421aa09a987" cao cao95.55.pdf "Civil Aviation Order" "95.55 Exemptions from CAR and CASR - Certain Light Sport Aircraft Lightweight Aeroplanes and Ultralight Aeroplanes) Instrument 2021"
  downloadLegislation "F2006L00838" "a1fa5c97-70bb-4ac3-9171-af0cfffc6d5a" cao cao95.56.pdf "Civil Aviation Order" "95.56 Exemption from provisions of the Civil Aviation Regulations 1988 - light sport aircraft"
  downloadLegislation "F2008L02849" "fcd2662a-a6d9-4d56-a17c-ff33024e46b1" cao cao95.57.pdf "Civil Aviation Order" "95.57 EADS CASA Airbus A330 multi-role transport tanker"
}

downloadAip() {
  if [ $# -lt 6 ]; then
    echo "arguments(6) <aip-dir> <aip-file> <out-dir> <out-file> <header> <name>"
    exit 8
  else
      out_dir=${download_dir}/${3}
      out_file=${out_dir}/${4}
      mkdir -p ${out_dir}
      wget -e robots=off https://www.airservicesaustralia.com/aip/current/${1}/${2} -O ${out_file}
      echo "${5},${6},${out_file}" >> ${index_file}
  fi
}

downloadAllAip() {
  downloadAip "aip" "complete_${aip_ersa}.pdf" aip/aip complete.pdf "Aeronautical Information Publication Book" "Complete"
  downloadAip "aip" "general_${aip_ersa}.pdf" aip/aip general.pdf "Aeronautical Information Publication Book" "General"
  downloadAip "aip" "enroute_${aip_ersa}.pdf" aip/aip enroute.pdf "Aeronautical Information Publication Book" "Enroute"
  downloadAip "aip" "aerodrome_${aip_ersa}.pdf" aip/aip aerodrome.pdf "Aeronautical Information Publication Book" "Aerodrome"
  downloadAip "aip" "cover_${aip_ersa}.pdf" aip/aip cover.pdf "Aeronautical Information Publication Book" "Amendment Instructions"
  downloadAip "ersa" "ersa_rds_index_${aip_ersa}.pdf" aip/ersa ersa_rds_index.pdf "Aeronautical Information Publication En Route Supplement Australia" "Complete"
}

downloadAdvisoryCircular() {
  if [ $# -lt 3 ]; then
    echo "arguments(3) <ac-file> <out-file> <name>"
    exit 8
  else
      out_dir=${download_dir}/advisory-circular
      out_file=${out_dir}/${2}
      mkdir -p ${out_dir}
      wget -e robots=off https://www.casa.gov.au/${1} -O ${out_file}
      echo "Advisory Circular,${3},${out_file}" >> ${index_file}
  fi
}

downloadAllAdvisoryCircular() {
  downloadAdvisoryCircular "flight-instructor-training" ac-61-07.pdf "AC 61-07 v1.0 Flight instructor training [Replaces CAAP 5.14-2]"
  downloadAdvisoryCircular "pilots-responsibility-collision-avoidance" ac-91-14.pdf "AC 91-14 v1.0 Pilots' responsibility for collision avoidance [Replaces CAAP 166-2(1)]"
  downloadAdvisoryCircular "restraints-infants-and-children" ac-91-18.pdf "AC 91-18 v1.1 Restraint of infants and children [Replaces CAAP 235-2(2)]"
  downloadAdvisoryCircular "upset-prevention-and-recovery-training" ac-121-03.pdf "AC 121-03 v1.0 Upset prevention and recovery training"
  downloadAdvisoryCircular "carriage-assistance-animals" ac-91-03.pdf "AC 91-03 v1.0 Carriage of assistance animals"
  downloadAdvisoryCircular "guidelines-aeroplanes-mtow-not-exceeding-5-700-kg-suitable-places-take-and-land" ac-91-02.pdf "AC 91-02 v1.1 Guidelines for aeroplanes with MTOW not exceeding 5 700 kg - suitable places to take off and land [Supports regulation 91.410 of CASR]"
  downloadAdvisoryCircular "wake-turbulence" ac-91-16.pdf "AC 91-16 v1.0 Wake turbulence"
  downloadAdvisoryCircular "aircraft-checklists" ac-91-22.pdf "AC 91-22 v2.0 Aircraft checklists"
  downloadAdvisoryCircular "passenger-safety-information" ac-91-19_121-04_133-10_135-12_138-10.pdf "AC 91-19 | 121-04 | 133-10 | 135-12 | 138-10 Passenger safety information [Replaces CAAP 253-2(2)]"
  downloadAdvisoryCircular "electronic-flight-bags-ac" ac-91-17.pdf "AC 91-17 v1.0 Electronic flight bags [Replaces CAAP 233-1(1)]"
  downloadAdvisoryCircular "fuel-and-oil-safety" ac-91-25.pdf "AC 91-25 v1.0 Fuel and oil safety"
  downloadAdvisoryCircular "download/ditching-ac" ac-91-09.pdf "AC 91-09 v1.0 Ditching [Replaces CAAP 253-1(1)]"
  downloadAdvisoryCircular "operations-vicinity-non-controlled-aerodromes" ac-91-10.pdf "AC 91-10 Operations in the vicinity of non-controlled aerodromes [Replaces CAAP 166-01]"
  downloadAdvisoryCircular "guidelines-aircraft-fuel-requirements" ac-91-15.pdf "AC 91-15 Guidelines for aircraft fuel requirements [Replaces CAAP 234-1]"
}

# https://www.casa.gov.au/sites/default/files/2021-08/caap-48-01-fatigue-management-for-flight-crew-members.pdf
#   CAAP 48-01 Fatigue management for flight crew members
downloadCaap() {
  if [ $# -lt 3 ]; then
    echo "arguments(3) <caap-file> <out-file> <name>"
    exit 8
  else
      out_dir=${download_dir}/caap
      out_file=${out_dir}/${2}
      mkdir -p ${out_dir}
      wget -e robots=off https://www.casa.gov.au/${1} -O ${out_file}
      echo "Civil Aviation Advisory Publication,${3},${out_file}" >> ${index_file}
  fi
}

downloadAllCaap() {
  downloadCaap "sites/default/files/2021-08/caap-48-01-fatigue-management-for-flight-crew-members.pdf" caap-48-01.pdf "CAAP 48-01 Fatigue management for flight crew members"
}

mkdir -p ${download_dir}
rm -f ${index_file}
downloadAllLegislation
downloadAllAip
downloadAllAdvisoryCircular
downloadAllCaap
