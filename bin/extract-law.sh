#!/bin/sh

set -e

if ! type pdftk >/dev/null ; then
  >&2 echo "Missing: pdftk" >&2
  exit 1
fi

if [ -z "$3" ]
then
  >&2 echo "arguments: <download-dir> <index-file> <extract-dir>"
  exit 33
else
  download_dir=${1}
  index_file=${2}
  extract_dir=${3}
fi

extractCAR1988_Complete() {
  car1988_complete_dir=${extract_dir}/car1988
  car1988_complete_file=${car1988_complete_dir}/car1988-complete.pdf
  mkdir -p ${car1988_complete_dir}

  pdftk \
    ${download_dir}/car1988/car1988-volume1.pdf \
    ${download_dir}/car1988/car1988-volume2.pdf \
    cat \
    output \
    ${car1988_complete_file}

    echo "Civil Aviation Regulations 1988,Complete,${car1988_complete_file}" >> ${index_file}
}

extractCASR1998_Complete() {
  casr1998_complete_dir=${extract_dir}/casr1998
  casr1998_complete_file=${casr1998_complete_dir}/casr1998-complete.pdf
  mkdir -p ${casr1998_complete_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume1.pdf \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    ${download_dir}/casr1998/casr1998-volume5.pdf \
    cat \
    output \
    ${casr1998_complete_file}

    echo "Civil Aviation Safety Regulations 1998,Complete,${casr1998_complete_file}" >> ${index_file}
}

extractCASR1998Part61Mos_Complete() {
  part61mos_complete_dir=${extract_dir}/casr1998/part61-manual-of-standards
  part61mos_complete_file=${part61mos_complete_dir}/casr1998-part61-manual-of-standards-complete.pdf
  mkdir -p ${part61mos_complete_dir}

  pdftk \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume1.pdf \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume2.pdf \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume3.pdf \
    ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume4.pdf \
    cat \
    output \
    ${part61mos_complete_file}

    echo "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards,Complete,${part61mos_complete_file}" >> ${index_file}
}

extractCASR1998Part61Mos() {
  if [ $# -lt 5 ]; then
    echo "arguments(5) <volume> <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    part61mos_parts_dir=${extract_dir}/casr1998/part61-manual-of-standards
    mkdir -p ${part61mos_parts_dir}

    pdftk ${download_dir}/casr1998/part61-manual-of-standards/casr1998-part61-manual-of-standards-volume${1}.pdf \
    cat \
    ${4}-${5} \
    output \
    ${part61mos_parts_dir}/${2}.pdf
    echo "Civil Aviation Safety Regulations 1998 Part 61 Manual of Standards,${3},${part61mos_parts_dir}/${2}.pdf" >> ${index_file}
  fi
}

extractCASR1998Part61Mos_Parts() {
  extractCASR1998Part61Mos \
    "1" \
    "casr1998-part61-manual-of-standards-volume1_part61-manual-of-standards" \
    "Volume 1 Part 61 Manual of Standards Instrument 2014" \
    "1" \
    "6"

  extractCASR1998Part61Mos \
    "1" \
    "casr1998-part61-manual-of-standards-volume1_schedule-1a" \
    "Volume 1 Schedule 1A" \
    "7" \
    "13"

  extractCASR1998Part61Mos \
    "1" \
    "casr1998-part61-manual-of-standards-volume1_directory-of-units-of-competency-and-units-of-knowledge" \
    "Volume 1 Directory of units of competency and units of knowledge" \
    "14" \
    "end"

  extractCASR1998Part61Mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_english-language-proficiency" \
    "Volume 2 Section 1 English Language Proficiency" \
    "4" \
    "6"

  extractCASR1998Part61Mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_common-standards" \
    "Volume 2 Section 2 Common Standards" \
    "7" \
    "20"

  extractCASR1998Part61Mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_navigation-and-instrument-flying-standards" \
    "Volume 2 Section 3 Navigation and Instrument Flying Standards" \
    "21" \
    "35"

  extractCASR1998Part61Mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_aircraft-rating-standards" \
    "Volume 2 Section 4 Aircraft Rating Standards" \
    "36" \
    "145"

  extractCASR1998Part61Mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_operational-rating-and-endorsement-standards" \
    "Volume 2 Section 5 Operational Rating and Endorsement Standards" \
    "146" \
    "236"

  extractCASR1998Part61Mos \
    "2" \
    "casr1998-part61-manual-of-standards-volume2_flight-activity-endorsement-standards" \
    "Volume 2 Section 6 Flight Activity Endorsement Standards" \
    "237" \
    "end"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1" \
    "Volume 3 Appendix 1 Flight crew licences and aircraft category ratings" \
    "5" \
    "188"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_basic-aeronautical-knowledge" \
    "Volume 3 Appendix 1 Section 1.1 Basic aeronautical knowledge (BAK)" \
    "5" \
    "19"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_general-aeronautical-knowledge" \
    "Volume 3 Appendix 1 Section 1.2 General aeronautical knowledge (AK)" \
    "20" \
    "36"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_aerodynamics" \
    "Volume 3 Appendix 1 Section 1.3 Aerodynamics (AD)" \
    "37" \
    "43"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_atpl-aircraft-general-knowledge" \
    "Volume 3 Appendix 1 Section 1.4 ATPL Aircraft General Knowledge (AG)" \
    "44" \
    "75"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_flight-rules-and-air-law" \
    "Volume 3 Appendix 1 Section 1.5 Flight Rules and Air Law (FR)" \
    "76" \
    "108"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_human-factors-principles" \
    "Volume 3 Appendix 1 Section 1.6 Human Factors Principles (HF)" \
    "109" \
    "124"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_navigation" \
    "Volume 3 Appendix 1 Section 1.7 Navigation (NV)" \
    "125" \
    "137"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_meteorology" \
    "Volume 3 Appendix 1 Section 1.8 Meteorology (MT)" \
    "138" \
    "155"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_operations-performance-and-planning" \
    "Volume 3 Appendix 1 Section 1.9 Operations Performance and Planning (OP)" \
    "156" \
    "172"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_flight-planning" \
    "Volume 3 Appendix 1 Section 1.10 Flight Planning (FP)" \
    "173" \
    "176"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-1_atpl-performance-and-loading" \
    "Volume 3 Appendix 1 Section 1.11 ATPL Performance and Loading (PL)" \
    "177" \
    "188"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2.pdf" \
    "Volume 3 Appendix 2 Operational Ratings" \
    "189" \
    "217"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_instrument-rating" \
    "Volume 3 Appendix 2 Section 2.1 Instrument Rating" \
    "189" \
    "196"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_private-ifr-rating" \
    "Volume 3 Appendix 2 Section 2.2 Private IFR Rating" \
    "197" \
    "202"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_aerial-application-rating-and-endorsements" \
    "Volume 3 Appendix 2 Section 2.3 Aerial Application Rating and Endorsements" \
    "203" \
    "208"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_instructor-ratings" \
    "Volume 3 Appendix 2 Section 2.4 Instructor Ratings" \
    "209" \
    "211"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_low-level-rating" \
    "Volume 3 Appendix 2 Section 2.5 Low-Level Rating" \
    "212" \
    "214"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_night-vision-imaging-systems-rating" \
    "Volume 3 Appendix 2 Section 2.6 Night Vision Imaging Systems (NVIS) Rating" \
    "215" \
    "215"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_night-vfr-rating" \
    "Volume 3 Appendix 2 Section 2.7 Night VFR Rating" \
    "216" \
    "216"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-2_examiner-ratings" \
    "Volume 3 Appendix 2 Section 2.8 Examiner Ratings" \
    "217" \
    "217"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-3" \
    "Volume 3 Appendix 3 Aircraft Ratings and Endorsements" \
    "218" \
    "229"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-3_class-ratings" \
    "Volume 3 Appendix 3 Section 3.1 Class Ratings" \
    "218" \
    "218"

  extractCASR1998Part61Mos \
    "3" \
    "casr1998-part61-manual-of-standards-volume3_appendix-3_type-ratings" \
    "Volume 3 Appendix 3 Section 3.2 Type Ratings" \
    "219" \
    "229"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_section-1-flight-crew-licence-and-associated-category-ratings" \
    "Volume 4 Section 1 Flight Crew Licence and Associated Category Ratings" \
    "4" \
    "9"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_operational-ratings" \
    "Volume 4 Section 2 Operational Ratings" \
    "10" \
    "10"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_foreign-licence-conversion" \
    "Volume 4 Section 3 Foreign Licence Conversion" \
    "11" \
    "12"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_australia-defence-force-conversion" \
    "Volume 4 Section 4 Australian Defence Force (ADF) Conversion" \
    "13" \
    "13"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-g-recreational-pilot-licence" \
    "Volume 4 Schedule 5 Section G Recreational Pilot Licence (RPL)" \
    "16" \
    "20"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-h-private-pilot-licence" \
    "Volume 4 Schedule 5 Section H Private Pilot Licence (PPL)" \
    "21" \
    "26"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-i-commercial-pilot-licence" \
    "Volume 4 Schedule 5 Section I Commercial Pilot Licence (CPL)" \
    "27" \
    "33"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-j-multi-crew-pilot-licence" \
    "Volume 4 Schedule 5 Section J Multi-Crew Pilot Licence (MPL)" \
    "34" \
    "36"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-k-air-transport-pilot-licence" \
    "Volume 4 Schedule 5 Section K Air Transport Pilot Licence (ATPL)" \
    "37" \
    "43"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-l-aircraft-ratings" \
    "Volume 4 Schedule 5 Section L Aircraft Ratings" \
    "44" \
    "63"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-m-instrument-rating" \
    "Volume 4 Schedule 5 Section M Instrument Rating" \
    "64" \
    "66"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-n-private-instrument-rating" \
    "Volume 4 Schedule 5 Section N Private Instrument Rating" \
    "67" \
    "70"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-o-night-vfr-rating" \
    "Volume 4 Schedule 5 Section O Night VFR Rating" \
    "71" \
    "73"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-p-night-vision-imaging-system-rating" \
    "Volume 4 Schedule 5 Section P Night Vision Imaging System (NVIS) Rating" \
    "74" \
    "76"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-q-low-level-rating" \
    "Volume 4 Schedule 5 Section Q Low-Level Rating" \
    "77" \
    "79"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-r-aerial-application-rating" \
    "Volume 4 Schedule 5 Section R Aerial Application Rating" \
    "80" \
    "83"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-t-pilot-instructor-ratings" \
    "Volume 4 Schedule 5 Section T Pilot Instructor Ratings" \
    "84" \
    "89"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-u-flight-examiner-rating" \
    "Volume 4 Schedule 5 Section U Flight Examiner Rating" \
    "90" \
    "92"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-v-flight-engineer-licence" \
    "Volume 4 Schedule 5 Section V Flight Engineer Licence" \
    "93" \
    "93"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-w-flight-engineer-type-rating" \
    "Volume 4 Schedule 5 Section W Flight Engineer Type Rating" \
    "94" \
    "94"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-x-flight-engineer-instructor-rating" \
    "Volume 4 Schedule 5 Section X Flight Engineer Instructor Rating" \
    "95" \
    "95"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-5-section-y-flight-engineer-examiner-rating" \
    "Volume 4 Schedule 5 Section Y Flight Engineer Examiner Rating" \
    "96" \
    "96"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-6-proficiency-check-standards" \
    "Volume 4 Schedule 6 Proficiency Check Standards" \
    "97" \
    "111"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-7-flight-review-standards" \
    "Volume 4 Schedule 7 Flight Review Standards" \
    "112" \
    "122"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-8-section-1-flight-tolerances" \
    "Volume 4 Schedule 8 Section 1 Flight Tolerances" \
    "123" \
    "134"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-8-section-2-english-language-proficiency-rating-scales" \
    "Volume 4 Schedule 8 Section 2 English Language Proficiency Rating Scales" \
    "135" \
    "137"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_schedule-9-cplh-training-for-non-integrated-training-courses" \
    "Volume 4 Schedule 9 CPL(H) training for paragraph 61.615 (1B) (b) (non-integrated training courses)" \
    "138" \
    "138"

  extractCASR1998Part61Mos \
    "4" \
    "casr1998-part61-manual-of-standards-volume4_note-to-part61-manual-of-standards" \
    "Volume 4 Note to Part 61 Manual of Standards (MOS)" \
    "139" \
    "139"
}

extractCASR1998Part91Mos() {
  if [ $# -lt 4 ]; then
    echo "arguments(4) <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    part91mos_parts_dir=${extract_dir}/casr1998/part91-manual-of-standards
    mkdir -p ${part91mos_parts_dir}

    pdftk ${download_dir}/casr1998/part91-manual-of-standards/casr1998-part91-manual-of-standards.pdf \
    cat \
    ${3}-${4} \
    output \
    ${part91mos_parts_dir}/${1}.pdf
    echo "Civil Aviation Safety Regulations 1998 Part 91 Manual of Standards,${2},${part91mos_parts_dir}/${1}.pdf" >> ${index_file}
  fi
}

extractCASR1998Part91Mos_Parts() {
  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_contents" \
    "Contents" \
    "3" \
    "10"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter1-preliminary" \
    "Chapter 01 Preliminary" \
    "11" \
    "27"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter2-prescriptions-for-certain-definitions-in-the-casr-dictionary" \
    "Chapter 02 Prescriptions for Certain Definitions in the CASR Dictionary" \
    "28" \
    "37"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter3-nvis-flights" \
    "Chapter 03 NVIS Flights" \
    "38" \
    "44"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter4-all-flights-airspeed-limits" \
    "Chapter 04 All Flights - Airspeed Limits" \
    "45" \
    "45"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter5-journey-logs-flights-that-begin-or-end-outside-australian-territory" \
    "Chapter 05 Journey Logs - Flights That Begin or End Outside Australian Territory" \
    "46" \
    "46"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter6-flying-in-formation" \
    "Chapter 06 Flying in Formation" \
    "47" \
    "47"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter7-flight-preparation-weather-assessments-requirements" \
    "Chapter 07 Flight Preparation (Weather Assessments) Requirements" \
    "48" \
    "49"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter8-flight-preparation-alternate-aerodromes-requirements" \
    "Chapter 08 Flight Preparation (Alternate Aerodrome) Requirements" \
    "50" \
    "55"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter9-flight-notifications" \
    "Chapter 09 Flight Notifications" \
    "56" \
    "57"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter10-matters-to-be-checked-before-take-off" \
    "Chapter 10 Matters to be Checked Before Take-off" \
    "58" \
    "60"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter11-air-traffic-services-prescribed-requirements" \
    "Chapter 11 Air Traffic Services - Prescribed Requirements" \
    "61" \
    "73"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter12-minimum-height-rules" \
    "Chapter 12 Minimum Height Rules" \
    "74" \
    "74"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter13-vfr-flights" \
    "Chapter 13 VFR Flights" \
    "75" \
    "75"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter14-ifr-flights" \
    "Chapter 14 IFR Flights" \
    "76" \
    "81"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter15-ifr-take-off-and-landing-minima" \
    "Chapter 15 IFR Take-off and Landing Minima" \
    "82" \
    "87"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter16-approach-ban-for-ifr-flights" \
    "Chapter 16 Approach Ban for IFR Flights" \
    "88" \
    "88"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter17-designated-non-controlled-aerodromes" \
    "Chapter 17 Designated Non-controlled Aerodromes" \
    "89" \
    "89"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter18-safety-when-aeroplane-operating-on-the-ground" \
    "Chapter 18 Safety When Aeroplane Operating on the Ground" \
    "90" \
    "90"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter19-fuel-requirements" \
    "Chapter 19 Fuel Requirements" \
    "91" \
    "95"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter20-safety-of-persons-and-cargo-on-aircraft" \
    "Chapter 20 Safety of Persons and Cargo on Aircraft" \
    "96" \
    "100"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter21-radio-frequency-broadcast-and-reporting-requirements" \
    "Chapter 21 Radio Frequency Broadcast and Reporting Requirements" \
    "101" \
    "106"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter22-performance-based-navigation-pbn" \
    "Chapter 22 Performance-based Navigation (PBN)" \
    "107" \
    "107"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter23-interception-of-aircraft" \
    "Chapter 23 Interception of Aircraft" \
    "108" \
    "108"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter24-take-off-performance" \
    "Chapter 24 Take-off Performance" \
    "109" \
    "110"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter25-landing-performance" \
    "Chapter 25 Landing Performance" \
    "111" \
    "112"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter26-equipment" \
    "Chapter 26 Equipment" \
    "113" \
    "166"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter27-experimental-and-light-sport-aircraft-placards" \
    "Chapter 27 Experimental and Light Sport Aircraft Placards" \
    "167" \
    "167"

  extractCASR1998Part91Mos \
    "casr1998-part91-manual-of-standards-part91-manual-of-standards_chapter28-requirements-for-minimum-equipment-lists" \
    "Chapter 28 Requirements for Minimum Equipment Lists" \
    "168" \
    "170"
}

extractCASR1998Part101Mos() {
  if [ $# -lt 4 ]; then
    echo "arguments(4) <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    part101mos_parts_dir=${extract_dir}/casr1998/part101-manual-of-standards
    mkdir -p ${part101mos_parts_dir}

    pdftk ${download_dir}/casr1998/part101-manual-of-standards/casr1998-part101-manual-of-standards.pdf \
    cat \
    ${3}-${4} \
    output \
    ${part101mos_parts_dir}/${1}.pdf
    echo "Civil Aviation Safety Regulations 1998 Part 101 Manual of Standards,${2},${part101mos_parts_dir}/${1}.pdf" >> ${index_file}
  fi
}

extractCASR1998Part101Mos_Parts() {
  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter1" \
    "a. CHAPTER 1 Preliminary" \
    "8" \
    "17"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter2" \
    "b. CHAPTER 2 RePL Training Course" \
    "18" \
    "40"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter4" \
    "c. CHAPTER 4 Operations in Controlled Airspace - Controlled Aerodromes" \
    "42" \
    "45"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter5" \
    "d. CHAPTER 5 RPA Operations Beyond VLOS" \
    "46" \
    "53"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter9" \
    "e. CHAPTER 9 Operations of RPA in Prescribed Areas" \
    "55" \
    "60"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter10" \
    "f. CHAPTER 10 Record Keeping for Certain RPA" \
    "61" \
    "71"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter12" \
    "g. CHAPTER 12 Identification of RPA and Model Aircraft" \
    "73" \
    "73"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter13" \
    "h. CHAPTER 13 Operation of Foreign Registered RPA and Model Aircraft" \
    "74" \
    "75"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter14" \
    "i. CHAPTER 14 Permissible Modifications to Registered RPA and Model Aircraft" \
    "76" \
    "76"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_chapter15" \
    "j. CHAPTER 15 Conduct of Online Training and Examinations for Accreditation" \
    "77" \
    "78"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_schedule1" \
    "k. Schedule 1 Acronyms and abbreviations" \
    "79" \
    "80"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_schedule2" \
    "l. Schedule 2 Directory for aeronautical knowledge standards for a RePL training course" \
    "81" \
    "82"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_schedule3" \
    "m. Schedule 3 Directory for practical competency standards for a RePL training course" \
    "83" \
    "84"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_schedule4" \
    "n. Schedule 4 Aeronautical knowledge units " \
    "85" \
    "114"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_schedule5" \
    "o. Schedule 5 Practical competency units" \
    "115" \
    "167"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_schedule6" \
    "p. Schedule 6 Flight Test Standards" \
    "168" \
    "194"

  extractCASR1998Part101Mos \
    "casr1998-part101-manual-of-standards_note_to_part101" \
    "q. Note to Part 101 (Unmanned Aircraft and Rockets) Manual of Standards 2019" \
    "195" \
    "196"
}

extractCASR1998Part61() {
  part61_dir=${extract_dir}/casr1998
  part61_file=${part61_dir}/casr1998-part61.pdf
  mkdir -p ${part61_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    89-331 \
    output \
    ${part61_file}

    echo "Civil Aviation Safety Regulations 1998,Part 61 Flight crew licensing,${part61_file}" >> ${index_file}
}

extractCASR1998Part91() {
  part91_dir=${extract_dir}/casr1998
  part91_file=${part91_dir}/casr1998-part91.pdf
  mkdir -p ${part91_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    467-593 \
    output \
    ${part91_file}

    echo "Civil Aviation Safety Regulations 1998,Part 91 General Operating and Flight Rules,${part91_file}" >> ${index_file}
}

extractCASR1998Part99() {
  part99_dir=${extract_dir}/casr1998
  part99_file=${part99_dir}/casr1998-part99.pdf
  mkdir -p ${part99_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    35-104 \
    output \
    ${part99_file}

    echo "Civil Aviation Safety Regulations 1998,Part 99 Drug and alcohol management plans and testing,${part99_file}" >> ${index_file}
}

extractCASR1998Part101() {
  part101_dir=${extract_dir}/casr1998
  part101_file=${part101_dir}/casr1998-part101.pdf
  mkdir -p ${part101_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    105-178 \
    output \
    ${part101_file}

    echo "Civil Aviation Safety Regulations 1998,Part 101 Unmanned aircraft and rockets,${part101_file}" >> ${index_file}
}

extractCASR1998Part103() {
  part103_dir=${extract_dir}/casr1998
  part103_file=${part103_dir}/casr1998-part103.pdf
  mkdir -p ${part103_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    179-195 \
    output \
    ${part103_file}

    echo "Civil Aviation Safety Regulations 1998,Part 103 Sport and recreation aircraft,${part103_file}" >> ${index_file}
}

extractCASR1998Part135() {
  part135_dir=${extract_dir}/casr1998
  part135_file=${part135_dir}/casr1998-part135.pdf
  mkdir -p ${part135_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume3.pdf \
    cat \
    524-581 \
    output \
    ${part135_file}

    echo "Civil Aviation Safety Regulations 1998,Part 135 Australian air transport operations-smaller aeroplanes,${part135_file}" >> ${index_file}
}

extractCASR1998Part137() {
  part137_dir=${extract_dir}/casr1998
  part137_file=${part137_dir}/casr1998-part137.pdf
  mkdir -p ${part137_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    31-64 \
    output \
    ${part137_file}

    echo "Civil Aviation Safety Regulations 1998,Part 137 Aerial application operations-other than rotorcraft,${part137_file}" >> ${index_file}
}

extractCASR1998Part138() {
  part138_dir=${extract_dir}/casr1998
  part138_file=${part138_dir}/casr1998-part138.pdf
  mkdir -p ${part138_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    65-131 \
    output \
    ${part138_file}

    echo "Civil Aviation Safety Regulations 1998,Part 138 Aerial work operations,${part138_file}" >> ${index_file}
}

extractCASR1998Part141() {
  part141_dir=${extract_dir}/casr1998
  part141_file=${part141_dir}/casr1998-part141.pdf
  mkdir -p ${part141_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    182-215 \
    output \
    ${part141_file}

    echo "Civil Aviation Safety Regulations 1998,Part 141 Recreational private and commercial pilot flight training other than certain integrated training courses,${part141_file}" >> ${index_file}
}

extractCASR1998Part142() {
  part142_dir=${extract_dir}/casr1998
  part142_file=${part142_dir}/casr1998-part142.pdf
  mkdir -p ${part142_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    216-265 \
    output \
    ${part142_file}

    echo "Civil Aviation Safety Regulations 1998,Part 142 Integrated and multi-crew pilot flight training contracted training and contracted checking,${part142_file}" >> ${index_file}
}

extractCAO_Complete() {
  cao_complete_dir=${extract_dir}/cao
  cao_complete_file=${cao_complete_dir}/cao-complete.pdf
  mkdir -p ${cao_complete_dir}

  pdftk \
    ${download_dir}/cao/cao20.2.pdf \
    ${download_dir}/cao/cao20.3.pdf \
    ${download_dir}/cao/cao20.4.pdf \
    ${download_dir}/cao/cao20.6.pdf \
    ${download_dir}/cao/cao20.7.1.pdf \
    ${download_dir}/cao/cao20.7.1B.pdf \
    ${download_dir}/cao/cao20.7.2.pdf \
    ${download_dir}/cao/cao20.9.pdf \
    ${download_dir}/cao/cao20.10.pdf \
    ${download_dir}/cao/cao20.10.1.pdf \
    ${download_dir}/cao/cao20.11.pdf \
    ${download_dir}/cao/cao20.13.pdf \
    ${download_dir}/cao/cao20.16.1.pdf \
    ${download_dir}/cao/cao20.16.2.pdf \
    ${download_dir}/cao/cao20.16.3.pdf \
    ${download_dir}/cao/cao20.17.pdf \
    ${download_dir}/cao/cao20.18.pdf \
    ${download_dir}/cao/cao20.21.pdf \
    ${download_dir}/cao/cao20.22.pdf \
    ${download_dir}/cao/cao20.91.pdf \
    ${download_dir}/cao/cao29.1.pdf \
    ${download_dir}/cao/cao29.2.pdf \
    ${download_dir}/cao/cao29.3.pdf \
    ${download_dir}/cao/cao29.4.pdf \
    ${download_dir}/cao/cao29.5.pdf \
    ${download_dir}/cao/cao29.6.pdf \
    ${download_dir}/cao/cao29.8.pdf \
    ${download_dir}/cao/cao29.10.pdf \
    ${download_dir}/cao/cao29.11.pdf \
    ${download_dir}/cao/cao40.2.2.pdf \
    ${download_dir}/cao/cao40.7.pdf \
    ${download_dir}/cao/cao45.0.pdf \
    ${download_dir}/cao/cao48.1.pdf \
    ${download_dir}/cao/cao82.0.pdf \
    ${download_dir}/cao/cao82.1.pdf \
    ${download_dir}/cao/cao82.7.pdf \
    ${download_dir}/cao/cao95.4.1.pdf \
    ${download_dir}/cao/cao95.7.pdf \
    ${download_dir}/cao/cao95.7.2.pdf \
    ${download_dir}/cao/cao95.7.3.pdf \
    ${download_dir}/cao/cao95.9.pdf \
    ${download_dir}/cao/cao95.14.pdf \
    ${download_dir}/cao/cao95.19.pdf \
    ${download_dir}/cao/cao95.20.pdf \
    ${download_dir}/cao/cao95.22.pdf \
    ${download_dir}/cao/cao95.23.pdf \
    ${download_dir}/cao/cao95.26.pdf \
    ${download_dir}/cao/cao95.27.pdf \
    ${download_dir}/cao/cao95.28.pdf \
    ${download_dir}/cao/cao95.29.pdf \
    ${download_dir}/cao/cao95.30.pdf \
    ${download_dir}/cao/cao95.31.pdf \
    ${download_dir}/cao/cao95.34.pdf \
    ${download_dir}/cao/cao95.55.pdf \
    ${download_dir}/cao/cao95.56.pdf \
    ${download_dir}/cao/cao95.57.pdf \
    cat \
    output \
    ${cao_complete_file}

    echo "Civil Aviation Order,Complete,${cao_complete_file}" >> ${index_file}
}

extractCAO_CLWA() {
  cao_clwa_dir=${extract_dir}/clwa
  cao_clwa_file=${cao_clwa_dir}/cao.pdf
  mkdir -p ${cao_clwa_dir}

  pdftk \
    ${download_dir}/cao/cao20.2.pdf \
    ${download_dir}/cao/cao20.4.pdf \
    ${download_dir}/cao/cao20.7.2.pdf \
    ${download_dir}/cao/cao20.9.pdf \
    ${download_dir}/cao/cao20.10.pdf \
    ${download_dir}/cao/cao20.11.pdf \
    ${download_dir}/cao/cao20.16.1.pdf \
    ${download_dir}/cao/cao20.16.2.pdf \
    ${download_dir}/cao/cao20.16.3.pdf \
    ${download_dir}/cao/cao20.18.pdf \
    ${download_dir}/cao/cao29.5.pdf \
    ${download_dir}/cao/cao48.1.pdf \
    cat \
    output \
    ${cao_clwa_file}

    echo "CLWA Exam Extract,Civil Aviation Order,${cao_clwa_file}" >> ${index_file}
}

extractCASR1998Part61_PIRC() {
  part61_pirc_dir=${extract_dir}/pirc
  part61_pirc_file=${part61_pirc_dir}/casr1998-part61.pdf
  mkdir -p ${part61_pirc_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume2.pdf \
    cat \
    270-290 \
    output \
    ${part61_pirc_file}

    echo "PIRC Exam Extract, Civil Aviation Safety Regulations 1998 Part 61,${part61_pirc_file}" >> ${index_file}
}

extractCASR1998Part141_SubpartE() {
  part141_subpartE_dir=${extract_dir}/pirc
  part141_subpartE_file=${part141_subpartE_dir}/casr1998-part141-subpartE.pdf
  mkdir -p ${part141_subpartE_dir}

  pdftk \
    ${download_dir}/casr1998/casr1998-volume4.pdf \
    cat \
    201-203 \
    output \
    ${part141_subpartE_file}

    echo "Part 141 operators-instructors, Civil Aviation Safety Regulations 1998 Part 141 Subpart 141.E,${part141_subpartE_file}" >> ${index_file}
}

decryptAIP() {
  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/complete.pdf \
    ${download_dir}/aip/aip/complete_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Complete (decrypted),${download_dir}/aip/aip/complete_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/general.pdf \
    ${download_dir}/aip/aip/general_decrypted.pdf \

  echo "Aeronautical Information Publication Book,General (decrypted),${download_dir}/aip/aip/general_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/enroute.pdf \
    ${download_dir}/aip/aip/enroute_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Enroute (decrypted),${download_dir}/aip/aip/enroute_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/aerodrome.pdf \
    ${download_dir}/aip/aip/aerodrome_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Aerodrome (decrypted),${download_dir}/aip/aip/aerodrome_decrypted.pdf" >> ${index_file}

  qpdf \
    --decrypt \
    ${download_dir}/aip/aip/cover.pdf \
    ${download_dir}/aip/aip/cover_decrypted.pdf \

  echo "Aeronautical Information Publication Book,Amendment Instructions (decrypted),${download_dir}/aip/aip/cover_decrypted.pdf" >> ${index_file}
}

decryptERSA() {
  qpdf \
    --decrypt \
    ${download_dir}/aip/ersa/ersa_rds_index.pdf \
    ${download_dir}/aip/ersa/ersa_rds_index_decrypted.pdf \

  echo "Aeronautical Information Publication En Route Supplement Australia,Complete (decrypted),${download_dir}/aip/ersa/ersa_rds_index_decrypted.pdf" >> ${index_file}
}

extractERSA() {
  if [ $# -lt 4 ]; then
    echo "arguments(4) <out-file> <name> <page-from> <page-to>"
    exit 9
  else
    ersa_parts_dir=${extract_dir}/aip/ersa
    mkdir -p ${ersa_parts_dir}

    pdftk \
      ${download_dir}/aip/ersa/ersa_rds_index_decrypted.pdf \
      cat \
      ${3}-${4} \
      output \
      ${ersa_parts_dir}/${1}.pdf

      echo "Aeronautical Information Publication En Route Supplement Australia,${2},${ersa_parts_dir}/${1}.pdf" >> ${index_file}
  fi
}

extractERSA_Parts() {
  extractERSA \
    "ersa_fac_index" \
    "a. FAC - INDEX OF AERODROMES" \
    "7" \
    "12"

  extractERSA \
    "ersa_avfax" \
    "b. AVFAX - AVFAX MADE SIMPLE" \
    "13" \
    "14"

  extractERSA \
    "ersa_intro" \
    "c. INTRO - ERSA INTRODUCTION" \
    "15" \
    "37"

  extractERSA \
    "ersa_fac" \
    "d. FACILITIES" \
    "39" \
    "805"

  extractERSA \
    "ersa_prd_areas" \
    "e. PRD AREAS" \
    "807" \
    "820"

  extractERSA \
    "ersa_aerodrome_ala_codes" \
    "f. AERODROME AND ALA CODES" \
    "821" \
    "876"

  extractERSA \
    "ersa_ifr_waypoints" \
    "g. IFR WAYPOINTS" \
    "877" \
    "901"

  extractERSA \
    "ersa_vfr_waypoints" \
    "h. VFR WAYPOINTS" \
    "903" \
    "930"

  extractERSA \
    "ersa_gen_flight_plan_requirements" \
    "i. GEN - FPR - FLIGHT PLAN REQUIREMENTS" \
    "931" \
    "977"

  extractERSA \
    "ersa_gen_special_procedures" \
    "j. GEN - SP - SPECIAL PROCEDURES" \
    "979" \
    "1006"

  extractERSA \
    "ersa_fis_preflight" \
    "k. GEN - PF - FIS: PREFLIGHT" \
    "1007" \
    "1023"

  extractERSA \
    "ersa_appendix_a_preflight" \
    "l. GEN - PF - A - APPENDIX A PREFLIGHT" \
    "1025" \
    "1039"

  extractERSA \
    "ersa_appendix_b_preflight" \
    "m. GEN - PF - B - APPENDIX B PREFLIGHT" \
    "1041" \
    "1046"

  extractERSA \
    "ersa_appendix_c_preflight" \
    "n. GEN - PF - C - APPENDIX C PREFLIGHT" \
    "1047" \
    "1051"

  extractERSA \
    "ersa_fis_in_flight" \
    "o. GEN - FIS - IN FLIGHT" \
    "1053" \
    "1060"

  extractERSA \
    "ersa_navigation_and_communication" \
    "q. NAV/COMM - NAVIGATION AND COMMUNICATION" \
    "1061" \
    "1063"

  extractERSA \
    "ersa_emergency_procedures" \
    "r. EMERG - EMERGENCY PROCEDURES" \
    "1065" \
    "1087"

  extractERSA \
    "ersa_rds" \
    "s. RDS - RUNWAY DISTANCE SUPPLEMENT" \
    "1089" \
    "1205"

  extractERSA \
    "ersa_index" \
    "t. INDEX" \
    "1207" \
    "end"
}

extractCAR1988_Complete
extractCASR1998_Complete
extractCASR1998Part61
extractCASR1998Part91
extractCASR1998Part99
extractCASR1998Part101
extractCASR1998Part103
extractCASR1998Part135
extractCASR1998Part137
extractCASR1998Part138
extractCASR1998Part141
extractCASR1998Part142
extractCASR1998Part61Mos_Complete
extractCASR1998Part61Mos_Parts
extractCASR1998Part91Mos_Parts
extractCASR1998Part101Mos_Parts
extractCAO_Complete
extractCAO_CLWA
extractCASR1998Part61_PIRC
extractCASR1998Part141_SubpartE
decryptAIP
decryptERSA
extractERSA_Parts
